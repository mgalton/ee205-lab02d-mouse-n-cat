###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 02d - mouseNcat - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Build a mouseNcat program
###
### @author  Mariko Galton <mgalton@hawaii.edu>
### @date    27_Jan_2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = mouseNcat

all: $(TARGET)

mouseNcat: mouseNcat.cpp
	$(CC) $(CFLAGS) -o $(TARGET) mouseNcat.cpp
clean:
	rm -f $(TARGET) *.o

